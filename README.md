# LIsA

GitLab Issue Board Automater.

## Intro

LIsA is a tool that can be used to automatically transition issues
across columns on a GitLab Issue Board.

## Actions

### To ~"In dev"

It walks through all the open issues assigned to the current user and
adds the ~"In dev" label if a MR that closes the issue exists.

### To ~"In review"

It walks through all merge requests created by the current user and
checks if it is assigned to a user different than the current user. If
it does, the issue that will be closed by the merge request is
assigned the ~"In review" label.

### To _Done_

It walks through all issues assigned to current user and removes the
labels ~"In dev" and ~"In review".
