all: lisa

fmt: lisa.go
	gofmt -s -w $<

run: lisa
	./$<

clean:
	rm -rf lisa

lisa: *.go
	go build $<

public/: public/index.html public/style.css

public/index.html: CHANGES.html
	mkdir -p $(@D)
	cat templates/min.html $< > $@

public/%.css: %.css
	cp $< $@


.PHONY: all fmt run clean
